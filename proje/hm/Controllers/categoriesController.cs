﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using hm.Models;
namespace hm.Controllers
{
    public class categoriesController : Controller
    {
        // GET: categories
        public ActionResult Index(int ID)
        {
            categoriesContext categoryContext = new categoriesContext();
            categories category= categoryContext.category.Single(ctg => ctg.id == ID);
            return View(category);
        }
    }
}