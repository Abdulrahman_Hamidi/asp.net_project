﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace hm.Models
{

    [Table("categories")]
    public class categories
    {
        public int id { get; set; }
        public int ParentId { get; set; }
        public string name { get; set; }

    }
}